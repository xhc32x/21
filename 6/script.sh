#!/usr/bin/bash
CURL=$(which curl)
THRESHOLD=85 #percents
DISK_BLOCKS_FULL_SIZE=$(df | awk '{print $2}' | sort -nr | head -n1)
USED_BLOCKS_DISK_SPACE=$(df | awk '{print $3}' | sort -nr | head -n1)
THRESHOLD_SIZE=`expr $DISK_BLOCKS_FULL_SIZE \* $THRESHOLD`
BOT_TOKEN=BOT_TOKEN

if [ $USED_BLOCKS_DISK_SPACE -ge $THRESHOLD_SIZE ]; then
    $CURL -k1 --header 'Content-Type: application/json' --request 'POST' \
    --data '{"chat_id":"-345812137","text":"WARNING DISK SPACE IS LESS THAN 15 PERCENTS!"}' \
    "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" &> /dev/null
fi