variable "region" {
  type        = string
  default     = "eu-north-1"
  description = "The AWS region where resources have been deployed"
}

variable "vpc_cidr_block" {
  default = "172.16.0.0/16"
}

variable "local_ip" {
  default = "0.0.0.0/0"
}

variable "user" {
  default = "ec2-user"
}

variable "environment" {
  default = "production"
}

variable "subnets_ips" {
  type    = list(any)
  default = ["172.16.1.0/24", "172.16.2.0/24", "172.16.3.0/24"]
}
