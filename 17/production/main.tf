terraform {
  backend "s3" {
    bucket = "ecs-bryi"
    key    = "21/17/production/terraform.tfstate"
    region = "us-east-1"
  }
}

#DATA

data "aws_availability_zones" "available" {
}

#VPC

module "vpc-prod" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name                         = "ansible-vpc-${var.environment}"
  cidr                         = var.vpc_cidr_block
  azs                          = data.aws_availability_zones.available.names
  create_database_subnet_group = true
  public_subnets               = var.subnets_ips

  enable_nat_gateway   = false
  single_nat_gateway   = false
  enable_dns_hostnames = true
}

#SECURITY GROUPS

resource "aws_security_group" "servers" {
  name        = "SG-${var.environment}"
  description = "allow inbound access from the internal aws services only"
  vpc_id      = module.vpc-prod.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = [var.local_ip]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 9090
    to_port     = 9090
    cidr_blocks = [var.local_ip]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 3000
    to_port     = 3000
    cidr_blocks = [var.local_ip]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 9100
    to_port     = 9100
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [module.vpc-prod]
}

#INSTANCES

data "aws_ami" "latest_amazon_linux" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

resource "aws_instance" "servers" {
  count           = 2
  ami             = data.aws_ami.latest_amazon_linux.id
  instance_type   = "t3.micro"
  security_groups = [aws_security_group.servers.id]
  subnet_id       = module.vpc-prod.public_subnets[count.index % length(module.vpc-prod.public_subnets)]
  key_name        = "saz-north"

  tags = {
    Name = "PrometheusTask-${var.environment}"
    Env  = var.environment
  }

  depends_on = [
    module.vpc-prod,
    aws_security_group.servers
  ]
}

# generate inventory file for Ansible
resource "local_file" "hosts_cfg" {
  content = templatefile("${path.module}/../ansible/inventory.tpl",
    {
      prometheus_server = tolist([aws_instance.servers[0].public_ip])
      prometheus_node   = tolist([aws_instance.servers[1].public_ip])
      vars = {
        "ansible_user"                 = "${var.user}"
        "ansible_ssh_private_key_file" = ("/home/bryi/21/17/ssh/saz-north.pem")
      }
    }
  )
  filename = "${path.module}/../ansible/inventory-${var.environment}"
  depends_on = [
    aws_instance.servers
  ]
}