#!/usr/bin/bash

terraform_fcn() {
    terraform init
    terraform fmt
    terraform validate
    terraform plan
    terraform apply --auto-approve
}

cd stage
terraform_fcn
cd ../production
terraform_fcn
cd ../ansible

for env in stage production; do
    ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ec2-user -i inventory-$env playbook.yml
done;

cd ..