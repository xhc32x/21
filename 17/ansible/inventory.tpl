[prometheus]
%{ for ip in prometheus_server ~}
${ip}
%{ endfor ~}

[node]
%{ for ip in prometheus_node ~}
${ip}
%{ endfor ~}

[all:vars]
%{ for vars_key, vars_value in vars }
${vars_key} = ${vars_value}
%{ endfor ~}