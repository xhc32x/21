#!/usr/bin/bash

cd stage
terraform destroy --auto-approve
cd ../production
terraform destroy --auto-approve
cd ..