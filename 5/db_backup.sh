#!/usr/bin/bash

DB_NAME=django
DB_USER=ctm
HOST=localhost
LOG_DIR=/var/log/backup
LOG=backup.log
LOGFILE=$LOG_DIR/$LOG
SCRIPT_NAME=`basename "$0"`
TEST_COMMAND=``

create_log_dir() {
    if [ ! -d $LOG_DIR ]; then
        mkdir -p $LOG_DIR
        echo "[ $SCRIPT_NAME ] [ `date +%Y-%m-%d-%H-%M` ] - Log file created!" >> $LOGFILE
    fi
}

echo "[ $SCRIPT_NAME ] [ `date +%Y-%m-%d-%H-%M` ] - Checking log dir and file existing..." >> $LOGFILE || create_log_dir

echo "[ $SCRIPT_NAME ] [ `date +%Y-%m-%d-%H-%M` ] - Backing up database..." >> $LOGFILE

pg_dump -U $DB_USER -d $DB_NAME -h $HOST | gzip > $DB_NAME-$(date "+%Y-%m-%d").sql.gz

echo "[ $SCRIPT_NAME ] [ `date +%Y-%m-%d-%H-%M` ] - Database successfully backed up!" >> $LOGFILE