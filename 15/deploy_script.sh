#!/usr/bin/bash

aws cloudformation create-stack \
  --stack-name env-infrastructure \
  --template-body file://template.yaml