#!/usr/bin/bash

aws cloudformation create-stack \
  --stack-name ecs \
  --capabilities CAPABILITY_NAMED_IAM \
  --template-body file://template.yaml