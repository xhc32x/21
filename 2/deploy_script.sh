#!/usr/bin/bash
IMAGE_NAME=nginximg
docker build -t $IMAGE_NAME .
sudo iptables -A INPUT -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 80 -m conntrack --ctstate ESTABLISHED -j ACCEPT
docker run --restart=always -d -p 80:80 $IMAGE_NAME