#!/usr/bin/bash

aws cloudformation create-stack \
  --stack-name ec2vpc \
  --template-body file://template.yaml