#!/usr/bin/bash

AWS_DEFAULT_REGION=us-east-1
DOCKER_REGISTRY=249736386209.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com
APP_NAME=nginx

#create ecr repo

cd ecr-repo
terraform init
terraform validate
terraform apply --auto-approve -var "repo_name=$APP_NAME"

#build and push docker app
cd ../app

aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $DOCKER_REGISTRY
docker build --cache-from $DOCKER_REGISTRY/$APP_NAME -t $DOCKER_REGISTRY/$APP_NAME .
docker push $DOCKER_REGISTRY/$APP_NAME

#create ecs cluster

cd ../ecs
terraform init
terraform validate
terraform apply --auto-approve -var "image=$DOCKER_REGISTRY/$APP_NAME:latest"

cd ..