variable "region" {
  default = "us-east-1"
}

variable "repo_name" {
  default = "nginx"
}

terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region  = var.region
  profile = ""
}

terraform {
  backend "s3" {
    bucket = "ecs-bryi"
    key    = "21/8/terraform-ecr.tfstate"
    region = "us-east-1"
  }
}

resource "aws_ecr_repository" "tf-ecr" {
  name = var.repo_name
}