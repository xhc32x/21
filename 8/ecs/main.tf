terraform {
  backend "s3" {
    bucket = "ecs-bryi"
    key    = "21/8/terraform-ecs.tfstate"
    region = "us-east-1"
  }
}

#DATA

data "aws_availability_zones" "available" {
}

data "aws_acm_certificate" "domain" {
  domain = "bryidomain.tk"
}

#VPC

module "vpc-prod" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name                         = var.app_name
  cidr                         = var.vpc_cidr_block
  azs                          = data.aws_availability_zones.available.names
  create_database_subnet_group = true
  private_subnets              = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets               = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    App = "${var.app_name}"
  }
}

#SECURITY GROUPS

resource "aws_security_group" "ecs_tasks" {
  name        = "${var.app_name}-ecs-main"
  description = "allow inbound access from the internal aws services only"
  vpc_id      = module.vpc-prod.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [module.vpc-prod]
}

resource "aws_security_group" "alb-sg" {
  name        = "alb-sg-${var.environment}"
  description = "allow inbound access from the ECS only"
  vpc_id      = module.vpc-prod.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [module.vpc-prod]
}

#ENDPOINTS 

# ECR
resource "aws_vpc_endpoint" "ecr_dkr" {
  vpc_id              = module.vpc-prod.vpc_id
  service_name        = "com.amazonaws.${var.region}.ecr.dkr"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  subnet_ids          = module.vpc-prod.private_subnets

  security_group_ids = [
    aws_security_group.ecs_tasks.id,
  ]

  tags = {
    Name        = "ECR Docker VPC Endpoint Interface - ${var.environment}"
    Environment = var.environment
  }

  depends_on = [
    module.vpc-prod,
    aws_security_group.ecs_tasks
  ]
}

# ECR
resource "aws_vpc_endpoint" "ecr_api" {
  vpc_id              = module.vpc-prod.vpc_id
  service_name        = "com.amazonaws.${var.region}.ecr.api"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  subnet_ids          = module.vpc-prod.private_subnets

  security_group_ids = [
    aws_security_group.ecs_tasks.id,
  ]

  tags = {
    Name        = "ECR API VPC Endpoint Interface - ${var.environment}"
    Environment = var.environment
  }

  depends_on = [
    module.vpc-prod,
    aws_security_group.ecs_tasks
  ]

}

# CloudWatch
resource "aws_vpc_endpoint" "cloudwatch" {
  vpc_id              = module.vpc-prod.vpc_id
  service_name        = "com.amazonaws.${var.region}.logs"
  vpc_endpoint_type   = "Interface"
  subnet_ids          = module.vpc-prod.private_subnets
  private_dns_enabled = true

  security_group_ids = [
    aws_security_group.ecs_tasks.id,
  ]

  tags = {
    Name        = "CloudWatch VPC Endpoint Interface - ${var.environment}"
    Environment = var.environment
  }

  depends_on = [
    module.vpc-prod,
    aws_security_group.ecs_tasks
  ]

}

resource "aws_vpc_endpoint" "s3" {
  vpc_id            = module.vpc-prod.vpc_id
  service_name      = "com.amazonaws.${var.region}.s3"
  vpc_endpoint_type = "Gateway"
  route_table_ids   = [module.vpc-prod.default_route_table_id]

  tags = {
    Name        = "S3 VPC Endpoint Gateway - ${var.environment}"
    Environment = var.environment
  }
}

#LOAD BALANCER 

resource "aws_lb" "alb" {
  name               = "${var.app_name}-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb-sg.id]
  subnets            = module.vpc-prod.public_subnets

  depends_on = [
    module.vpc-prod,
    aws_security_group.alb-sg
  ]
}

resource "aws_lb_listener" "alb_listener_ssl" {
  load_balancer_arn = aws_lb.alb.arn

  port     = 443
  protocol = "HTTPS"

  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = data.aws_acm_certificate.domain.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }

  depends_on = [
    aws_lb.alb
  ]
}

resource "aws_lb_listener" "alb_listener_http" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  depends_on = [
    aws_lb.alb
  ]
}

resource "aws_lb_target_group" "alb_target_group" {
  name        = "${var.app_name}-alb-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = module.vpc-prod.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = 2
    interval            = 30
    protocol            = "HTTP"
    unhealthy_threshold = 2
  }

  depends_on = [aws_lb.alb]

  lifecycle {
    create_before_destroy = true
  }
}

#ECS

resource "aws_ecs_cluster" "main" {
  name = var.app_name

  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  tags = {
    Name = var.app_name
  }
}

resource "aws_iam_role" "task_role" {
  name               = "ecs_tasks-${var.app_name}-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role" "main_ecs_tasks" {
  name               = "main_ecs_tasks-${var.app_name}-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "main_ecs_tasks" {
  name = "main_ecs_tasks-${var.app_name}-policy"
  role = aws_iam_role.main_ecs_tasks.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*"
            ],
            "Resource": ["*"]
        },
        {
            "Effect": "Allow",
            "Resource": [
              "*"
            ],
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:CreateLogGroup",
                "logs:DescribeLogStreams"
            ]
        }
    ]

}
EOF
}

resource "aws_ecs_task_definition" "nginx" {
  family                   = var.app_name
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : "${var.app_name}",
      image : var.image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      portMappings : [
        {
          containerPort : 80
          protocol : "tcp",
          hostPort : 80
        }
      ],
      logConfiguration : {
        logDriver : "awslogs",
        options : {
          awslogs-group : "awslogs-${var.app_name}",
          awslogs-region : "us-east-1",
          awslogs-stream-prefix : "awslogs-example",
          awslogs-create-group : "true"
        }
      },
    }
  ])
}

resource "aws_ecs_service" "nginx" {
  name                 = var.app_name
  cluster              = aws_ecs_cluster.main.id
  task_definition      = aws_ecs_task_definition.nginx.family
  force_new_deployment = true
  desired_count        = 1
  launch_type          = "FARGATE"

  network_configuration {
    security_groups = tolist([aws_security_group.ecs_tasks.id])
    subnets         = module.vpc-prod.private_subnets
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.alb_target_group.arn
    container_name   = var.app_name
    container_port   = 80
  }

  depends_on = [
    aws_ecs_cluster.main,
    aws_ecs_task_definition.nginx,
    aws_lb_target_group.alb_target_group
  ]
}

#ROUTE53

resource "aws_route53_zone" "primary_zone" {
  name = var.domain_name
}

resource "aws_route53_record" "main" {
  zone_id = aws_route53_zone.primary_zone.zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = aws_lb.alb.dns_name
    zone_id                = aws_lb.alb.zone_id
    evaluate_target_health = true
  }
}