variable "app_name" {
  type        = string
  default     = "nginx"
  description = "Name of the container application"
}

variable "region" {
  type        = string
  default     = "us-east-1"
  description = "The AWS region where resources have been deployed"
}

variable "image" {
  default = "249736386209.dkr.ecr.us-east-1.amazonaws.com/backend:latest"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "domain_name" {
  default = "bryidomain.tk"
}

variable "fargate_cpu" {
  default = 256
}

variable "fargate_memory" {
  default = 512
}

variable "environment" {
  default = "dev"
}