#!/usr/bin/bash

DB_NAME=django
DB_USER=ctm
HOST=localhost

pg_dump -U $DB_USER -d $DB_NAME -h $HOST | gzip > $DB_NAME-$(date "+%Y-%m-%d").sql.gz