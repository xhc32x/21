#!/usr/bin/python3

import json
import csv

with open('example.json') as json_file:
    data = json.load(json_file)
    with open('generated.csv', mode='w') as generated:
        csv_writer = csv.writer(generated, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(['KEY', 'VALUE'])
        for dict in data:
            for key,value in dict.items():
                csv_writer.writerow([f'{key}', f'{value}'])