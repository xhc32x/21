variable "region" {
  type        = string
  default     = "eu-north-1"
  description = "The AWS region where resources have been deployed"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "local_ip" {
  default = "0.0.0.0/0"
}

variable "user" {
  default = "ec2-user"
}
